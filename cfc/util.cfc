<cfcomponent name="util" output="false">

<cffunction name="getUser" access="package" returntype="String">
	<cfset result = "uctr" />
	<cfif isDefined("Session.r25user")>
		<cfset result = Session.r25user />
	</cfif>
	<cfset localLog("getUser: #result#") />
	<cfreturn result/>
</cffunction>

<cffunction name="localLog" access="public">
	<cfargument name="textMessage" type="String" required="yes"/>
	<cflog file="cal-uc-kiosk" text="#textMessage#" />
</cffunction>

<cffunction name="getTodayStart" access="package" returntype="String">
	<cfset today = now() />
	<!--- should be 'HH0000' --->
	<cfset result = "#DateFormat(today, 'yyyymmdd')#T#TimeFormat(today, 'HH0000')#"/>
	<cfreturn result />
</cffunction>

<cffunction name="getTodayEnd" access="package" returntype="String">
	<cfset today = now() />
	<cfset result = "#DateFormat(today, 'yyyymmdd')#T235959" />
	<cfreturn result />
</cffunction>


</cfcomponent>
