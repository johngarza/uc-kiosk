<cfcomponent name="login" output="false">
<cfobject name="util" component="util" />
<cfset login.AUTHENTICATION_REALM_BASIC = "Basic realm=""R25 WebServices""" />

<cffunction name="createSession" access="public" returntype="String">
	<cfargument name="r25wsSessStruct" type="Struct" required="Yes" />
	<cfset sessList = structKeyList(r25wsSessStruct) />
	<cfloop list="#sessList#" index="username">
		<cfset sessData = r25wsSessStruct[username] />
		<cfscript>
			createIndividualSession(sessData, username);
		</cfscript>
	</cfloop>
</cffunction>

<cffunction name="getSessionForUser" access="public" returntype="Struct">
	<cfargument name="username" type="String" required="Yes" />
	<cfset r25sess = StructNew() />
	<cflock type="Read" scope="Application" timeout="1">
		<cfset r25sessions = Application.appCache.r25wsSess />
	<cfif StructKeyExists(r25sessions, username)>
		<cfset r25sess = r25sessions["#username#"] />
	<cfelse>
		<!--- if we fail user lookup, let's default to the main.draft user --->
		<cfset username = "uctr" />
		<cfset r25sess = r25sessions["#username#"] />
	</cfif>
	</cflock>
	<cfif (StructKeyExists(r25sess, "JSESSIONID") AND StructKeyExists(r25sess, "WSSESSIONID"))>
		<!--- do nothing --->
	<cfelse>
		<!--- no session ids exists, let's create a session for this user --->
		createIndividualSession(r25sess, username);
	</cfif>
	<cfreturn r25sess />
</cffunction>

<cffunction name="constructAuthenticationResponse" access="private" returntype="String">
    <cfargument name="sessData" type="Struct" required="Yes" />
    <cfargument name="username" type="String" required="Yes" />
    <cfargument name="hcookies" type="Struct" required="Yes" />
    <cfargument name="challengeContent" type="String" required="Yes" />
    
    <cfscript>
        challengeDoc = XmlParse(challengeContent.Trim());
        //util.localLog("INFO finished parsing challengeContent");
        headercookies = structkeylist(hcookies['set-cookie']);
        //util.localLog("finished structkeylist");
    </cfscript>

    <!--- parse out any cookies from the http response --->
    <cfloop list="#headercookies#" index="key">
        <!---
        <cfset print=util.localLog("#listgetat(cfhttp.Responseheader['set-cookie']['#key#'],1,'=')#") />
        <cfset print=util.localLog("#listgetat(listgetat(cfhttp.Responseheader['set-cookie']['#key#'],2,'='),1,";")#") />
        --->
        <cfset "#listgetat(cfhttp.Responseheader['set-cookie']['#key#'],1,'=')#" = mid(cfhttp.Responseheader['set-cookie']['#key#'],find('=',cfhttp.Responseheader['set-cookie']['#key#'])+1,len(cfhttp.Responseheader['set-cookie']['#key#'])) />
        <cfset "#listgetat(cfhttp.Responseheader['set-cookie']['#key#'],1,'=')#" = listgetat(listgetat(cfhttp.Responseheader['set-cookie']['#key#'],2,'='),1,";") />
    </cfloop>
    <cfset print = util.localLog("INFO finished cfloop") />
                                                     
    <!--- construct our xml return document, encode the password --->
	<cfscript>
        challengeNode = XmlSearch(challengeDoc, "/*[name()='r25:login_challenge']/*[name()='r25:login']/*[name()='r25:challenge']");
        key = challengeNode[1].xmltext;
        if (key eq login.AUTHENTICATION_REALM_BASIC) {
            response = "Basic #ToBase64("#username#:#sessData.password#")#";
        } else {
            response = lcase(hash(lcase(hash(password,"MD5")) & ":" & key,"MD5"));
        }
        //util.localLog("INFO computed response: #response#");
    </cfscript>
	<cfsavecontent variable="loginXML"><?xml version="1.0" encoding="UTF-8"?>
<r25:login_challenge xmlns:r25="http://www.collegenet.com/r25" pubdate="<cfoutput>#dateformat(now(),"yyyy-mm-dd")#T#timeformat(now(),"HH:MM:SS")#-04:00</cfoutput>">
    <r25:login>
        <r25:challenge />
        <r25:username><cfoutput>#username#</cfoutput></r25:username>
        <r25:response><cfoutput>#response#</cfoutput></r25:response>
    </r25:login>
</r25:login_challenge></cfsavecontent>
    <cfreturn trim(loginXML) />
</cffunction>
        
<cffunction name="createIndividualSession" access="private" returntype="String">
    <cfargument name="sessData" type="Struct" required="Yes" />
    <cfargument name="username" type="String" required="Yes" />
    <cfset password = sessData.password />
    <cfset login_url = "#Application.baseroot#login.xml" />
    <cfset print = util.localLog("GET login.xml - login.createSession") />
	<!--- Grab login.xml document --->
	<cftry>
        <cfhttp url="#login_url#" port="#Application.port#" method="get" timeout="1"></cfhttp>
        <cfset hcookies = cfhttp.ResponseHeader />
        <cfset challengeContent = cfhttp.Filecontent />
        <cfset loginXML = constructAuthenticationResponse(sessData, username, hcookies, challengeContent) />
        <!---
        <cfset print = util.localLog("POST to login.xml - login.createSession") />
		<cfset print = util.localLog("#trim(loginXML)#") />
        --->
        <cfhttp url="#login_url#" port="#Application.port#" method="post" timeout="1">
            <cfhttpparam type="xml" value="#loginXML#" />
            <cfhttpparam type="cookie" name="WSSESSIONID" value="#WSSESSIONID#" />
            <cfhttpparam type="cookie" name="JSESSIONID" value="#JSESSIONID#"/>
        </cfhttp>
        <cfif (cfhttp.statuscode EQ "201 Created")>
            <cflock type="Exclusive" scope="Application" timeout="3">
                <cfset sessData.WSSESSIONID = WSSESSIONID />
                <cfset sessData.JSESSIONID = JSESSIONID />
                <cfset sessData.INITDT = now() />
            </cflock>
        </cfif>
    <cfcatch type="Any">
        <cfset print = util.localLog("ERROR: caught error while trying to log in! ERROR") />
        <cfset print = util.localLog(cfcatch.message) />
		<cflock type="Exclusive" scope="Application" timeout="1">
			<cfset sessData.WSSESSIONID = "ERROR" />
			<cfset sessData.JSESSIONID = "ERROR" />
			<cfset sessData.INITDT = now() />
		</cflock>
    </cfcatch>
    </cftry>
</cffunction>
</cfcomponent>
