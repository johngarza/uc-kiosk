<!---
cproxy.cfc

ColdFusion Component for pulling room info from R25WS
--->

<cfcomponent name="cproxy" output="false">
<cfobject name="util" component="util" />
<cfobject name="sess" component="login" />
<cfobject name="json" component="json" />
<cfset requestTimeout = "30" />
<!--- /** POST proxy, DO NOT CACHE **/--->
<cffunction name="putResourceXML" access="public" returntype="xml" output="true">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="resource_name" type="String" required="yes" />
	<cfargument name="content" type="String" required="yes" />
	<cfargument name="username" type="String" required="yes" />
	<cfset response = StructNew() />
	<cfset put_uri = "#Application.baseroot##resource_uri#" />
	<cfset data = "" />
	<cfhttp url="#put_uri#" port="80" method="PUT" timeout="#requestTimeout#">
		<cfhttpparam type="cookie" name="WSSESSIONID" value="#mySess.WSSESSIONID#" />
		<cfhttpparam type="cookie" name="JSESSIONID" value="#mySess.JSESSIONID#"/>
		<cfhttpparam type="header" name="Content-Type" value="text/xml"/>
		<cfhttpparam type="XML" value="#content.Trim()#"/>
	</cfhttp>
	<cfscript>
		try {
			util.localLog("PUT: #put_uri#");
			xmlStr = cfhttp.filecontent;
			xmlDoc = XmlParse(xmlStr.Trim());
		} catch (Exception e) {
			util.localLog("ERROR on PUT #put_uri#");
			util.localLog("#cfcatch.message#");
		}
	</cfscript>
	<cfreturn xmlDoc />
</cffunction>

<!--- /** POST proxy, DO NOT CACHE **/--->
<cffunction name="postResource" access="public" returntype="struct" output="false">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="resource_name" type="String" required="yes" />
	<cfargument name="content" type="String" required="yes" />
	<cfargument name="username" type="String" required="yes" />
	
	<!--- grab our existing R25WS session object from the Application scope --->
	<cfset mySess = sess.getSessionForUser(username) />
	<cfset response = StructNew() />
	<cfset post_uri = "#Application.baseroot##resource_uri#" />
	<cfset data = "" />
	<cfhttp url="#post_uri#" port="#Application.port#" method="POST" timeout="#requestTimeout#">
		<cfhttpparam type="cookie" name="WSSESSIONID" value="#mySess.WSSESSIONID#" />
		<cfhttpparam type="cookie" name="JSESSIONID" value="#mySess.JSESSIONID#"/>
		<cfhttpparam type="header" name="Content-Type" value="text/xml"/>
		<cfhttpparam type="XML" value="#content.Trim()#"/>
	</cfhttp>
	<cfscript>
		//util.localLog("POST Response - cproxy.postResource");
		//util.localLog(CFHTTP.FileContent);
		dataPush = StructNew();
		util.localLog("POST: #post_uri#");
		
		try {
			dataPush['data'] = json.decode(CFHTTP.FileContent);
			dataPush['status'] = "SUCCESS";
			//dataPush['pubdate'] = dataPush['data'][resource_name]["pubdate"];
			dataPush['pubdate'] = dataPush['data'][resource_name]._attr.pubdate;
		} catch (Exception e) {
			dataPush['data'] = e;
			dataPush['status'] = "ERROR";
		}
	</cfscript>
	<cfreturn dataPush />
</cffunction>

<!--- /** POST proxy, DO NOT CACHE **/--->
<cffunction name="postResourceXML" access="public" returntype="struct" output="false">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="resource_name" type="String" required="yes" />
	<cfargument name="content" type="String" required="yes" />
	<cfargument name="username" type="String" required="yes" />
	
	<!--- grab our existing R25WS session object from the Application scope --->
	<cfset mySess = sess.getSessionForUser(username) />
	<cfset response = StructNew() />
	<cfset post_uri = "#Application.baseroot##resource_uri#" />
	<cfhttp url="#post_uri#" port="#Application.port#" method="POST"  timeout="#requestTimeout#">
		<cfhttpparam type="cookie" name="WSSESSIONID" value="#mySess.WSSESSIONID#" />
		<cfhttpparam type="cookie" name="JSESSIONID" value="#mySess.JSESSIONID#"/>
		<cfhttpparam type="header" name="Content-type" value="text/plain; charset=UTF-8" />
		<cfhttpparam type="body" name="text/plain" value="#content.trim()#" />
	</cfhttp>
	<cfscript>
		//util.localLog("CONTENT: #content#");
		//util.localLog("POST Response - cproxy.postJSONResource");
		util.localLog("POST: #post_uri#");
		dataPush = StructNew();
		
		try {
			dataPush['raw'] = ToString(CFHTTP.FileContent);
			dataPush['data'] = ToString(CFHTTP.FileContent);
			//json.decode(CFHTTP.FileContent);
			dataPush['status'] = "SUCCESS";
			//dataPush['pubdate'] = dataPush['data'][resource_name]._attr.pubdate;
		} catch (Exception e) {
			dataPush['data'] = e;
			dataPush['status'] = "ERROR";
		}
		util.localLog("RESULT: #dataPush['raw']#");
	</cfscript>
	<cfreturn dataPush />
</cffunction>




<!--- /** POST proxy, DO NOT CACHE **/--->
<cffunction name="postJSONResource" access="public" returntype="struct" output="false">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="resource_name" type="String" required="yes" />
	<cfargument name="content" type="String" required="yes" />
	<cfargument name="username" type="String" required="yes" />
	
	<!--- grab our existing R25WS session object from the Application scope --->
	<cfset mySess = sess.getSessionForUser(username) />
	<cfset response = StructNew() />
	<cfset post_uri = "#Application.baseroot##resource_uri#" />
	<cfhttp url="#post_uri#" port="#Application.port#" method="POST" timeout="#requestTimeout#">
		<cfhttpparam type="cookie" name="WSSESSIONID" value="#mySess.WSSESSIONID#" />
		<cfhttpparam type="cookie" name="JSESSIONID" value="#mySess.JSESSIONID#"/>
		<cfhttpparam type="header" name="Content-type" value="text/plain" />
		<cfhttpparam type="body" name="text/plain" value="#content.trim()#" />
	</cfhttp>
	<cfscript>
		util.localLog("POST: #post_uri#");
		dataPush = StructNew();
		
		try {
			dataPush['raw'] = ToString(CFHTTP.FileContent);
			dataPush['data'] = json.decode(CFHTTP.FileContent);
			dataPush['status'] = "SUCCESS";
			dataPush['pubdate'] = dataPush['data'][resource_name]._attr.pubdate;
		} catch (Exception e) {
			dataPush['data'] = e;
			dataPush['status'] = "ERROR";
		}
	</cfscript>
	<cfreturn dataPush />
</cffunction>


<!--- private method to store a cached request into the application cache --->
<cffunction name="putResource" access="public" output="true">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="resource_name" type="String" required="yes" />
	<cfargument name="content" type="String" required="yes" />
	<cfargument name="username" type="String" required="yes" />
	
	<!--- grab our existing R25WS session object from the Application scope --->
	<cfset mySess = sess.getSessionForUser(username) />
	<cfset response = StructNew() />
	<cfset put_uri = "#Application.baseroot##resource_uri#" />
	<cfset data = "" />
	<cfhttp url="#put_uri#" port="#Application.port#" method="PUT" timeout="#requestTimeout#">
		<cfhttpparam type="cookie" name="WSSESSIONID" value="#mySess.WSSESSIONID#" />
		<cfhttpparam type="cookie" name="JSESSIONID" value="#mySess.JSESSIONID#"/>
		<cfhttpparam type="header" name="Content-type" value="text/plain" />
		<cfhttpparam type="body" name="text/plain" value="#content.trim()#" />
		</cfhttp>
	<cfscript>
		//util.localLog("PUT Response - cproxy.putResource");
		util.localLog("PUT: #put_uri#");
		util.localLog("CONTENT: #CFHTTP.FileContent#");
		dataPush = StructNew();
		
		try {
			dataPush['raw'] = ToString(CFHTTP.FileContent);
			dataPush['data'] = json.decode(CFHTTP.FileContent);
			dataPush['status'] = "SUCCESS";
			//dataPush['pubdate'] = dataPush['data'][resource_name]._attr.pubdate;
		} catch (Exception e) {
			dataPush['data'] = e;
			dataPush['status'] = "ERROR";
		}
	</cfscript>
	<cfreturn dataPush />
</cffunction>


<!--- /** UNCACHED GET proxy **/--->
<cffunction name="getResourceUncached" access="public" returntype="struct" output="false">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="resource_name" type="String" required="yes" />
	<cfargument name="username" type="String" required="yes"/>
	<cfscript>
	result = getResource(resource_uri, resource_name, username, "none");
	</cfscript>
	<cfreturn result />
</cffunction>

<!--- grab our existing R25WS session object from the Application scope --->
<!---
	<cfset mySess = sess.getSessionForUser(username) />
	<cfset get_uri = "#Application.baseroot##resource_uri#" />
	<cfhttp url = "#get_uri#" port="#Application.port#" method="GET" timeout="#requestTimeout#">
		<cfhttpparam type="cookie" name="WSSESSIONID" value="#mySess.WSSESSIONID#" />
		<cfhttpparam type="cookie" name="JSESSIONID" value="#mySess.JSESSIONID#"/>
	</cfhttp>
	<cfscript>
		dataPush = StructNew();
		util.localLog(get_uri);
		//util.localLog("CONTENT: " + CFHTTP.FileContent);
		try {
			dataPush['raw'] = ToString(CFHTTP.FileContent);
			dataPush['data'] = json.decode(CFHTTP.FileContent);
			dataPush['status'] = "SUCCESS";
			dataPush['pubdate'] = now();
		} catch (Exception e) {
			dataPush['data'] = e;
			dataPush['status'] = "ERROR";
			dataPush['pubdate'] = now();
		}
	</cfscript>
	<cfreturn dataPush />
</cffunction>
--->

<!--- /** CACHED GET proxy **/--->
<cffunction name="getResource" access="public" returntype="struct" output="false">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="resource_name" type="String" required="yes" />
	<cfargument name="username" type="String" required="yes"/>
	<cfargument name="timeout" type="String" required="no" />

	<!--- grab our existing R25WS session object from the Application scope --->
	<cfset defaultTimeout = "30" />
	<cfif isDefined('timeout')>
		<cfset defaultTimeout = timeout />
	</cfif>
	<cfset get_uri = "#Application.baseroot##resource_uri#&cache=#defaultTimeout#" />
	<cfset mySess = sess.getSessionForUser(username) />

	<cfset cachedResource = cacheResourceGet(resource_uri) />
	<cfset result = 0 />

	<cfhttp url = "#get_uri#" port="#Application.port#" method="GET"  timeout="#requestTimeout#">
		<cfhttpparam type="cookie" name="WSSESSIONID" value="#mySess.WSSESSIONID#" />
		<cfhttpparam type="cookie" name="JSESSIONID" value="#mySess.JSESSIONID#"/>
	</cfhttp>
	<cfscript>
		dataPush = StructNew();
		util.localLog(get_uri);
		util.localLog("resource name: #resource_name#");
		try {
			dataPush['data'] = json.decode(CFHTTP.FileContent);
			dataPush['raw'] = ToString(CFHTTP.FileContent);
			dataPush['pubdate'] = dataPush['data'][resource_name]._attr.pubdate;
			dataPush['status'] = "SUCCESS";
			util.localLog("dataPush: #dataPush['pubdate']#");
		} catch (Exception e) {
			dataPush['data'] = e;
			dataPush['pubdate'] = now();
			dataPush['status'] = "ERROR";
		}
		result = StructCopy(dataPush);
	</cfscript>
	<cfset cacheResourcePut(resource_uri, dataPush) />
	<cfreturn result />
</cffunction>

<!--- private method to retrieve cached GET requests from our application cache --->
<cffunction name="cacheResourceGet" access="private" returntype="struct" output="false">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfset returnStruct = StructNew() />
	<cfset keyName = "#util.getUser()#@#resource_uri#" />
	<cfscript>
		if (StructKeyExists(Application.appCache, keyName)) {
			returnStruct = Application.appCache[keyName];
			util.localLog("Grabbing data from appCache: #returnStruct['pubdate']#");
		} else {
			returnStruct = StructNew();
		}
	</cfscript>
	<cfreturn returnStruct />
</cffunction>

<!--- privaet method to store a cached request into the application cache --->
<cffunction name="cacheResourcePut" access="private" output="false">
	<cfargument name="resource_uri" type="String" required="yes"/>
	<cfargument name="dataPush" type="Struct" required="yes"/>
	<cfset keyName = "#util.getUser()#@#resource_uri#" />
	<cfscript>
		Application.appCache[keyName] = StructCopy(dataPush);
	</cfscript>
</cffunction>

</cfcomponent>