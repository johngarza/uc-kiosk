<!---
contacts.cfc

ColdFusion Component for pulling contact info from R25WS
--->
<cfcomponent name="contacts" output="false">
<cfobject name="cProxyObj" component="cproxy" />
<cfobject name="util" component="util" />

<cfset SearchContacts = "contacts.xml?scope=simple&otransform=json.xsl&lint=T&simple=T&organization_id=" />

<cffunction name="contacts" access="remote" returntype="array" returnformat="json" output="true">
	<cfargument name="organization_id" type="string" required="no" />
	<cfset arrayData = ArrayNew(1) />
	<cfscript>
		thisOrg = Application.Org;
		if (isDefined('organization_id')) {
			thisOrg = organization_id;
		}
		resource_uri = "#SearchContacts##thisOrg#";
		resource_name = "contacts";
		data = cProxyObj.getResource(resource_uri, resource_name, util.getUser());
		conts = data['data'].contacts;
		if (StructKeyExists(conts, "contact")) {
			myD = data['data'].contacts.contact;
		} else {
			myD = ArrayNew(1);
		}
	</cfscript>
	<cfif IsArray(myD)>
		<cfset arrayData = myD />
	<cfelse>
		<cfset arrayData[1] = myD />
	</cfif>
	<cfreturn arrayData />
</cffunction>
</cfcomponent>