<!---
reservations.cfc

ColdFusion Component for pulling event info from R25WS
--->
<cfcomponent name="rsrvs" output="false">
<cfobject name="cProxyObj" component="cproxy" />
<cfobject name="util" component="util" />

<cfset MajorEvents  = "reservations.xml?lint=T&scope=extended&include=event+spaces+resources+customers+text+workflow+attributes&space_query_id=211911&otransform=spomecs/json.xsl&" />
<cfset AllEvents    = "reservations.xml?lint=T&scope=extended&include=event+spaces+resources+customers+text+workflow+attributes&space_query_id=228997&otransform=spomecs/json.xsl&" />
<cfset SearchEvents = "reservations.xml?lint=T&scope=extended&include=event+spaces+resources+customers+text+workflow+attributes&otransform=spomecs/json.xsl&space_query_id=" />
<cfset Reservations = "reservation.xml?lint=T&scope=extended&include=event+spaces+resources+customers+text+workflow+attributes&otransform=spomecs/json.xsl&rsrv_id=" />

<cffunction name="spomecs" access="remote" returntype="array" returnformat="json" output="true">
	<cfargument name="space_query_id" type="string" required="yes" />
	<cfargument name="start_dt" type="string" required="yes" />
	<cfargument name="end_dt" type="string" required="yes" />
	
	<cfset sdt = DateFormat(ParseDateTime(start_dt), 'YYYYMMDD') />
	<cfset edt = DateFormat(ParseDateTime(end_dt), 'YYYYMMDD') />
	<cfset resource_uri = "#SearchEvents##space_query_id#&start_dt=#sdt#T000000&end_dt=#edt#T235959" />
	<cfset resource_name = "reservations" />
	<cfset arrayData = ArrayNew(1) />
	<cfscript>
		data = cProxyObj.getResource(resource_uri, resource_name, util.getUser());
		rsrvs = data['data'].reservations;
		if (StructKeyExists(rsrvs, "reservation")) {
			myD = data['data'].reservations.reservation;
		} else {
			myD = ArrayNew(1);
		}
	</cfscript>
	<cfif IsArray(myD)>
		<cfset arrayData = myD />
	<cfelse>
		<cfset arrayData[1] = myD />
	</cfif>
	<cfreturn arrayData />
</cffunction>

<cffunction name="rsrv" access="remote" returntype="array" returnformat="json" output="true">
	<cfargument name="reservation_id" type="string" required="yes" />
	<cfargument name="start_dt" type="string" required="yes" />
	<cfargument name="end_dt" type="string" required="yes" />
	
	<cfset sdt = DateFormat(ParseDateTime(start_dt), 'YYYYMMDD') />
	<cfset edt = DateFormat(ParseDateTime(end_dt), 'YYYYMMDD') />
	<cfset resource_uri = "#Reservations##reservation_id#&start_dt=#sdt#T000000&end_dt=#edt#T235959" />
	<cfset resource_name = "reservations" />
	<cfset arrayData = ArrayNew(1) />
	<cfscript>
	util.localLog("called getResource with uri: #resource_uri#");
		data = cProxyObj.getResourceUncached(resource_uri, resource_name, util.getUser());
		rsrvs = data['data'].reservations;
		if (StructKeyExists(rsrvs, "reservation")) {
			myD = data['data'].reservations.reservation;
		} else {
			myD = ArrayNew(1);
		}
	</cfscript>
	<cfif IsArray(myD)>
		<cfset arrayData = myD />
	<cfelse>
		<cfset arrayData[1] = myD />
	</cfif>
	<cfreturn arrayData />
</cffunction>
</cfcomponent>