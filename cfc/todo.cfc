<!---
reservations.cfc

ColdFusion Component for pulling event info from R25WS
--->
<cfcomponent name="todo" output="true">
<cfobject name="cProxyObj" component="cproxy" />
<cfobject name="util" component="util" />

<cfset CreateTodo  ="todo.xml" />
<cfset PutTodo = "todo.xml?todo_id=" /> 

<cffunction name="create" access="remote" returntype="xml" output="true">
	<cfargument name="due_date" type="string" required="yes" />
	<cfargument name="assigned_to" type="string" required="yes" />
	<cfargument name="assigned_by" type="string" required="yes" />
	<cfargument name="object_id" type="string" required="yes" />
	<cfargument name="comment" type="string" required="yes" />

	<cfset name = "SPOMECS #DateFormat(now(), 'MM-DD-YYYY')#" />
	<cfset ddt = DateFormat(ParseDateTime(due_date), 'YYYYMMDD') />
	<cfset type_id = "2" />
	<cfset priority_id = "1" />
	<cfset object_type = "1" />
	<cfset read = "F"/>
	<cfset current_state = "1" />

	<cfset resource_uri = "#CreateTodo#" />
	<cfset resource_name = "todo_create" />
	<cfset data = "" />
	<cfscript>
	//POST to #CreateTodo#
	//parse xml response
	todoRes = cProxyObj.postResourceXML(resource_uri, resource_name, " ", util.getUser());
	todoDoc = XmlParse(todoRes['raw'].trim());
	//parse out todo_id
	todo_id = todoDoc['r25:todo']['r25:todo_item']['r25:todo_id'].XmlText;
	//todo_id = "440952";
	util.localLog(todo_id);
	//set defaults
	todoDoc['r25:todo']['r25:todo_item']['r25:type_id'].XmlText = "#type_id#";
	todoDoc['r25:todo']['r25:todo_item']['r25:priority_id'].XmlText = "#priority_id#";
	todoDoc['r25:todo']['r25:todo_item']['r25:object_type'].XmlText = "#object_type#";
	todoDoc['r25:todo']['r25:todo_item']['r25:read'].XmlText = "#read#";
	todoDoc['r25:todo']['r25:todo_item']['r25:cur_state_id'].XmlText = "#current_state#";
	
	//modify xml object with given arguments
	todoDoc['r25:todo']['r25:todo_item']['r25:name'].XmlText = "#XmlFormat(name)#";
	todoDoc['r25:todo']['r25:todo_item']['r25:due_date'].XmlText = "#ddt#T120000";
	todoDoc['r25:todo']['r25:todo_item']['r25:assigned_to_id'].XmlText = "#assigned_to#";
	todoDoc['r25:todo']['r25:todo_item']['r25:assigned_by_id'].XmlText = "#assigned_by#";
	todoDoc['r25:todo']['r25:todo_item']['r25:object_id'].XmlText = "#object_id#";
	todoDoc['r25:todo']['r25:todo_item']['r25:comment'].XmlText = "#XmlFormat(comment)#";
	//PUT to #PutTodo##todo_id#
	//send response back as JSON
	resource_uri = "#PutTodo##todo_id#";
	resource_name = "todo_put_#todo_id#";
	util.localLog(ToString(todoDoc));
	todoPUT = cProxyObj.putResourceXML(resource_uri, resource_name, ToString(todoDoc), util.getUser());
	</cfscript>
	<cfreturn todoPUT />
</cffunction>

<!---
<cffunction name="spomecs" access="remote" returntype="array" returnformat="json" output="true">
	<cfargument name="space_query_id" type="string" required="yes" />
	<cfargument name="start_dt" type="string" required="yes" />
	<cfargument name="end_dt" type="string" required="yes" />
	
	<cfset sdt = DateFormat(ParseDateTime(start_dt), 'YYYYMMDD') />
	<cfset edt = DateFormat(ParseDateTime(end_dt), 'YYYYMMDD') />
	<cfset resource_uri = "#SearchEvents##space_query_id#&start_dt=#sdt#T000000&end_dt=#edt#T235959" />
	<cfset resource_name = "rsrvs_#space_query_id#_#sdt#_#edt#" />
	<cfset arrayData = ArrayNew(1) />
	<cfscript>
		data = cProxyObj.getResourceUncached(resource_uri, resource_name, util.getUser());
		rsrvs = data['data'].reservations;
		if (StructKeyExists(rsrvs, "reservation")) {
			myD = data['data'].reservations.reservation;
		} else {
			myD = ArrayNew(1);
		}
	</cfscript>
	<cfif IsArray(myD)>
		<cfset arrayData = myD />
	<cfelse>
		<cfset arrayData[1] = myD />
	</cfif>
	<cfreturn arrayData />
</cffunction>
--->

</cfcomponent>

