<!---
events.cfc

ColdFusion Component for pulling event info from R25WS
--->

<cfcomponent name="request" output="false">
<cfobject name="cProxyObj" component="cproxy" />
<cfobject name="util" component="util" />
<!--- otransform for R25WS 2.3 --->
<!--
<cfset MajorEvents = "rm_reservations.xml?lint=T&simple=T&include=reservations&scope=extended&space_query_id=483967&event_query_id=484569&otransform=json.xsl&" />
-->
<cfset AllEvents = "rm_reservations.xml?lint=T&simple=T&include=reservations&scope=extended&space_query_id=483969&otransform=json.xsl&" />
<!--- 
http://ut159630.utsarr.net/r25ws/wrd/run/
events.xml?
	space_query_id=211911&
	start_dt=20111017T125352&
	end_dt=20111017T235959&
	include=reservations&
	scope=extended --->

<cfset SpaceEvents = "rm_reservations.xml?otransform=json.xsl&space_id=" />

<cffunction name="spaceEvents" access="remote" returntype="array" returnformat="json" output="true">
	<cfargument name="space_id" type="string" required="yes" />
	<cfset start_dt = util.getTodayStart() />
	<cfset end_dt = util.getTodayEnd() />
	<cfset resource_uri = "#SpaceEvents##space_id#&start_dt=#start_dt#&end_dt=#end_dt#" />
	<cfset resource_name = "rm_reservations_for_#space_id#" />
	<cfset arrayData = ArrayNew(1) />
	<cfscript>
		data = cProxyObj.getResource(resource_uri, resource_name, util.getUser());
		rsrvs = data['data'].space_reservations;
		if (StructKeyExists(rsrvs, "space_reservation")) {
			myD = data['data'].space_reservations.space_reservation;
			if (isArray(myD)) {
				arrayData = myD;
			} else {
				ArrayAppend(arrayData, myD);
			}
		} else {
			myD = ArrayNew(1);
		}
	</cfscript>
	<cfreturn arrayData />
</cffunction>

<cffunction name="majorEvents" access="remote" returntype="array" returnformat="json" output="true">
	<cfset start_dt = util.getTodayStart() />
	<cfset end_dt = util.getTodayEnd() />
	<cfset resource_uri = "#AllEvents#start_dt=#start_dt#&end_dt=#end_dt#" />
	<cfset resource_name = "major_spaces" />
	<cfset arrayData = ArrayNew(1) />
	<cfscript>
		data = cProxyObj.getResource(resource_uri, resource_name, util.getUser());
		rsrvs = data['data'].space_reservations;
		if (StructKeyExists(rsrvs, "space_reservation")) {
			myD = data['data'].space_reservations.space_reservation;
			if (isArray(myD)) {
				arrayData = myD;
			} else {
				ArrayAppend(arrayData, myD);
			}
		}
	</cfscript>
	<cfreturn arrayData />
</cffunction>

<cffunction name="allEvents" access="remote" returntype="array" returnformat="json" output="true">
	<cfset start_dt = util.getTodayStart() />
	<cfset end_dt = util.getTodayEnd() />
	<cfset resource_uri = "#AllEvents#start_dt=#start_dt#&end_dt=#end_dt#" />
	<cfset resource_name = "space_reservations" />
	<cfset arrayData = ArrayNew(1) />
	<cfscript>
		data = cProxyObj.getResource(resource_uri, resource_name, util.getUser());
		rsrvs = data['data'].space_reservations;
		if (StructKeyExists(rsrvs, "space_reservation")) {
			myD = data['data'].space_reservations.space_reservation;
			if (isArray(myD)){ 
				arrayData = myD;
			} else {
				ArrayAppend(arrayData, myD);
			}
		}
	</cfscript>
	<cfreturn arrayData />
</cffunction>

</cfcomponent>

