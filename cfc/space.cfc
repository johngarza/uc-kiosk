<!---
space.cfc

ColdFusion Component for pulling room info from R25WS
--->
<cfcomponent name="space" output="false">
<cfobject name="cProxyObj" component="cproxy" />
<cfobject name="util" component="util" />

<cfset SpaceSearches = "space_searches.xml?favorites=T&otransform=json.xsl" />
<cfset Spaces = "spaces.xml?otransform=json.xsl&query_id=" />

<cffunction name="spaceSearches" access="remote" returntype="array" returnformat="wddx" output="true">
	<cfset resource_uri = SpaceSearches />
	<cfset resource_name = "space_searches" />
	<cfscript>
		data = cProxyObj.getResource(resource_uri, resource_name, util.getUser());
		arrayData = data['data'].space_searches.space_search;
	</cfscript>
	<cfreturn arrayData />
</cffunction>

<cffunction name="spaces" access="remote" returntype="array" returnformat="json" output="true">
	<cfargument name="query_id" type="String" required="yes" />
	<cfset resource_uri = "#Spaces##query_id#" />
	<cfset resource_name = "spaces" />
	<cfscript>
		data = cProxyObj.getResource(resource_uri, resource_name, util.getUser());
		arrayData = data['data'].spaces.space;
	</cfscript>
	<cfreturn arrayData />
</cffunction>


</cfcomponent>

