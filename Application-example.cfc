<cfcomponent output="false">
	<!--- Application name, should be unique --->
	<cfset this.name = "<YOUR APP NAME GOES HERE>">
	<!--- How long application vars persist --->
	<cfset this.applicationTimeout = createTimeSpan(0,0,0,5)>
	<!--- Should client vars be enabled? --->
	<cfset this.clientManagement = true>
	<!--- Where should we store them, if enable? --->
	<cfset this.clientStorage = "cookie">
	<!--- Where should cflogin stuff persist --->
	<cfset this.loginStorage = "session">
	<!--- Should we even use sessions? --->
	<cfset this.sessionManagement = true>
	<!--- How long do session vars persist? --->
	<cfset this.sessionTimeout = createTimeSpan(0,0,0,5)>
	<!--- Should we set cookies on the browser? --->
	<cfset this.setClientCookies = true>
	<!--- should cookies be domain specific, ie, *.foo.com or www.foo.com --->
	<cfset this.setDomainCookies = false>
	<!--- should we try to block 'bad' input from users --->
	<cfset this.scriptProtect = "none">
	<!--- should we secure our JSON calls? --->
	<cfset this.secureJSON = false>
	<!--- Should we use a prefix in front of JSON strings? --->
	<cfset this.secureJSONPrefix = "">
	<!--- Used to help CF work with missing files and dir indexes --->
	<cfset this.welcomeFileList = "">
	
	<!--- define custom coldfusion mappings. Keys are mapping names, values are full paths  --->
	<cfset this.mappings = structNew()>
	<!--- define a list of custom tag paths. --->
	<cfset this.customtagpaths = "">
	<cfobject name="r25sessObj" component="cfc.login" />
	
	<!--- Run when application starts up --->
	<cffunction name="onApplicationStart" returnType="boolean" output="false">
		<cfset Application.dateInitialized = now() />
		<!--- application variables --->
		<cfset Application.r25server = "<YOUR_R25WS_HOSTNAME>" />
		<cfset Application.port = "<YOUR_R25WS_PORT>" />
		
		<cfset Application.baseinvocation = "<YOUR FULL URL TO THE CFC DIRECTORY>" />
		<cfset Application.baseroot = "http://#Application.r25server#/r25ws/wrd/run/" />
		<cfset Application.appCache= StructNew() />
        <!--- credentials --->
        <cfset r25wsSess = StructNew() />
        <cfset r25wsSess['<R25_WS_USERNAME>'] = StructNew() />
        <cfset r25wsSess['<R25_WS_USERNAME>'].password = "<R25_WS_PASSWORD>" />
        <cfscript>
                r25sessObj.createSession(r25wsSess);
        </cfscript>
        <cfset Application.appCache["r25wsSess"] = r25wsSess />
		<cfset Application.UCOpsSpaces = "#Application.baseroot#spaces.xml?query_id=483969&otransform=json.xsl" />
		<cfset Application.SPOMECSSpaces = "#Application.baseroot#spaces.xml?query_id=211911&otransform=json.xsl" />
		<cfset Application.SpaceReservations = "#Application.baseroot#rm_reservations.xml?otransform=json.xsl&space_id=" />
		<cfreturn true>
	</cffunction>

	<!--- Run when application stops --->
	<cffunction name="onApplicationEnd" returnType="void" output="false">
		<cfargument name="applicationScope" required="true">
	</cffunction>

	<!--- Fired when user requests a CFM that doesn't exist. --->
	<cffunction name="onMissingTemplate" returnType="boolean" output="false">
		<cfargument name="targetpage" required="true" type="string">
		<cfreturn true>
	</cffunction>
	
	
	<!--- Run before the request is processed --->
	<cffunction name="onRequestStart" access="public" returnType="boolean" output="true">
		<cfargument name="thePage" type="string" required="true" />

		<!--- maintenance code --->
		<!---
		<cfset var LOCAL = StructNew() />
		<cfheader statuscode="503" statustext="Service Temporarily Unavailable"/>
		<cfheader name="retry-after" value="3600" />
		<cfinclude template="cfm/down.cfm" />
		<cfreturn false />
		--->

		<!--- live code --->
		<cfscript>
			initDiff = dateDiff("s", application.dateInitialized, now());
		</cfscript>
		<cfif (initDiff GT 10800)>
			<cfset OnApplicationStart()>
		</cfif>
		<cfif StructKeyExists(URL,"app_force_refresh")>
			<cfset OnApplicationStart()>
		</cfif>
		<cfreturn true>
	</cffunction>

</cfcomponent>