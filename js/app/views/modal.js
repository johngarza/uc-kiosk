$(function() {
  window.ModalView = Backbone.View.extend({
    template: 'modal',

    render: function() {
      console.log('polling modal.html for content!');
      var _this = this;
      $.get('modal.html', function(modalHTML) {
        _this.template = _.template(modalHTML);
        $(_this.el).html(_this.template());
      });

      return this;
    }
  });
});
