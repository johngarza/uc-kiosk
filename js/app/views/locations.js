$(function(){
	window.LocationsView = Backbone.View.extend({
		template: _.template($('#locations-template').html()),
		_location_data: null,
		events: {
			"click .room" : "gotoRoom",
			"click .office" : "gotoOffice",
			"click .lounge" : "gotoLounge",
			"click .dining" : "gotoDining",
			"click .service" : "gotoService"
		},
		initialize: function(options) {
			this._location_data = options.location_data;
		},
		render: function() {
			var templateJSON = this.model.toJSON();
			templateJSON["location_data"] = this._location_data.toJSON();
			$(this.el).html(this.template(templateJSON));
			if (this.model.get("current_location")) {
				this.model.set({current_location_name: null, current_location: null});
			}
		},
		gotoRoom: function(evt) {
			var target_id = $(evt.target).attr("data-id");
			var room_info = this._location_data.get("rooms");
			var location = room_info[target_id];
			var nav = "#map/room/" + target_id;
			this.model.set({"floor" : location.floor, "current_location": location, "current_location_name" : target_id});
			app.navigate(nav, true);
		},
		gotoOffice: function(evt) {
			var target_id = $(evt.target).attr("data-id");
			var room_info = this._location_data.get("offices");
			var location = room_info[target_id];
			var nav = "#map/office/" + target_id;
			this.model.set({"floor" : location.floor, "current_location": location, "current_location_name" : target_id});
			app.navigate(nav, true);
		},
		gotoLounge: function(evt) {
			var target_id = $(evt.target).attr("data-id");
			var room_info = this._location_data.get("lounges");
			var location = room_info[target_id];
			var nav = "#map/lounge/" + target_id;
			this.model.set({"floor" : location.floor, "current_location": location, "current_location_name" : target_id});
			app.navigate(nav, true);
		},
		gotoDining: function(evt) {
			var target_id = $(evt.target).attr("data-id");
			var room_info = this._location_data.get("dining");
			var location = room_info[target_id];
			var nav = "#map/dining/" + target_id;
			this.model.set({"floor" : location.floor, "current_location": location, "current_location_name" : target_id});
			app.navigate(nav, true);
		},
		gotoService: function(evt) {
			var target_id = $(evt.target).attr("data-id");
			var room_info = this._location_data.get("services");
			var location = room_info[target_id];
			var nav = "#map/service/" + target_id;
			this.model.set({"floor" : location.floor, "current_location": location, "current_location_name" : target_id});
			app.navigate(nav, true);
		}
	});
});
