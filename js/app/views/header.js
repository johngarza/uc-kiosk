$(function(){
	window.HeaderView = Backbone.View.extend({
		template: _.template($('#header-template').html()),
		events: {
			//"click #closeSetup" : "closeSetup",
			"click .menu" : "menuChange"
		},
		initialize: function() {
			_.bindAll(this, "render");
			this.model.bind('change', this.render);
		},
		render: function() {
			$(this.el).html(this.template(this.model.toJSON()));
			var headerViewThis = this;
			return this;
		},
		menuChange: function(evt) {
			var target = $(evt.target).attr("id");
			app.navigate(target, true);
		}
	});
});
