$(function(){
	window.FooterView = Backbone.View.extend({
		template: _.template($('#footer-template').html()),
		events: {
			"click .locationMenu":"filterLocation"
			//"click #closeSetup" : "closeSetup",
		},
		initialize:function() {
			_.bindAll(this, "render");
			this.model.bind('change', this.render);
		},
		render: function() {
			$(this.el).html(this.template(this.model.toJSON()));
			var footerViewThis = this;
		},
		unhighlightAll: function(evt) {
			_.each($(".room"), function(val, idx) {
				$(val).removeClass("active");
			});
			_.each($(".office"), function(val, idx) {
				$(val).removeClass("active");
			});
			_.each($(".lounge"), function(val, idx) {
				$(val).removeClass("active");
			});
			_.each($(".dining"), function(val, idx) {
				$(val).removeClass("active");
			});
			_.each($(".service"), function(val, idx) {
				$(val).removeClass("active");
			});
		},
		filterLocation: function(evt) {
			var target_filter = $(evt.target).attr("id");
			this.model.set({filter:target_filter});
			this.unhighlightAll();
			_.each($("." + target_filter + ".roomName"), function(val, idx) {
				$(val).addClass("active");
			});
		}
	});
});
