$(function(){
	window.EventsView = Backbone.View.extend({
		template: _.template($('#events-template').html()),
		_location_data: null,
		_event_data: null,
		events: {
			"click .roomLookup" : "roomLookup"
		},
		updateEvents: function(new_event_data) {
			this._event_data = new_event_data;
			this._event_data.sort(startCompare);
		},
		initialize: function(options) {
			var eventsViewThis = this;
			this._location_data = options.location_data;
			_.bindAll(this, "render");
			this.model.bind("change", this.render);
		},
		roomLookup: function(evt) {
			var room_obj = null, lounge_obj = null;
			var room_id = $(evt.target).parent().attr("data-space-id");
			var rooms = this._location_data.get("rooms");
			var lounges = this._location_data.get("lounges");
			var nav = "#map/";

			var room_key = null, room_obj = null;
			for (var key in rooms) {
				if (rooms[key]["id"] == room_id) {
					room_obj = rooms[key];
					room_key = key;
				}
			}
			var lounge_key = null, lounge_obj = null;
			for (var key in lounges) {
				if ( lounges[key]["id"] == room_id) {
					 lounge_obj =  lounges[key];
					 lounge_key = key;
				}
			}
			if (room_obj) {
				nav = nav + "room/" + room_key;
				this.model.set({"floor" : room_obj.floor, "current_location": room_obj, "current_location_name" : room_key});
			}
			if (lounge_obj) {
				nav = nav + "lounge/" + lounge_key;
				this.model.set({"floor" : lounge_obj.floor, "current_location": lounge_obj, "current_location_name" : lounge_key});
			}
			app.navigate(nav, true);
		},
		render: function() {
			var current_view = this.model.get("current_view");
			if (current_view == "events") {
				var renderJson = this.model.toJSON();
				renderJson['event_data'] = this._event_data;
				$(this.el).html(this.template(renderJson));
			}
		}
	});
});
