$(function(){
	window.MapView = Backbone.View.extend({
		template: _.template($('#map-template').html()),
		events: {
			"click .toggleFloor" : "toogleFloor"
		},
		initialize : function() {
		},
		toogleFloor : function() {
			var newFloor = "1";
			if (this.model.get("floor") == newFloor) {
				newFloor = "2";
			}
			this.model.set({"floor" : newFloor});
			this.render();
		},
		render : function() {
			var current_kiosk = this.model.get("kiosk");
			var kiosk_setup = this.model.get("kiosks")["kiosk" + current_kiosk];
			var pop_target = "#kiosk" + current_kiosk;
			var current_location = this.model.get("current_location");
			
			$(this.el).html(this.template(this.model.toJSON()));
			
			$(pop_target).popover({
				title: kiosk_setup.title,
				content: kiosk_setup.content,
				animation: true,
				placement: kiosk_setup.placement
			});
			$(pop_target).popover("show");

			if (current_location) {
				var target_loc = "#Pos" + this.model.get("current_location_name");
				var loc = this.model.get("current_location");
				$(target_loc).popover({
					content: loc.room,
					title: loc.name,
					animation: true,
					placement : loc.placement
				});
				$(target_loc).popover("show");
			}
			return this;
		}
	});
});
