Backbone.sync = (function() {
	var previous = Backbone.sync;
	var sync = function(method, model, options) {
		var store = model.lawnchair || model.collection.lawnchair;
		var json = model.toJSON();
		var thisID = json.id;
		var res = null;
		json.key = json.id;
		
		var checkExistence = function(record) {
			if (record) {
				res = record;
			}
		};
		
		var callback = function(record) {
			if (record == null) {
				options.error("Record not found");
				return
			}

			var keyToId = function(obj) {
				obj.id = model.id || obj.key;   // workaround for Lawnchair issue #57
				delete obj.key;
			};

			if (_.isArray(record)) {
				_.each(record, keyToId);
			} else {
				keyToId(record);
			}
			options.success(model, record, options);
			//options.success(record);
		};

		//delete json.id;

		switch (method) {
			case "read":
				json.key ? store.get(json.key, callback) : store.all(callback);
				//store.all(callback);
				break;
			case "create":
				store.get(json.id, checkExistence);
				if (!res) {
					store.nuke();
					//beers.save({key:thisobj.key,value:obj});
					resp = store.save(json, callback);
				} else {
					resp = store.get(json.key, callback);
				}
				break;
			case "update":
				json.key = json.id;
				resp = store.save(json);
				break;
			case "delete":
				//console.log("LS DELETE! " + json.key);
				resp = store.remove(json.key);
				//, options.success);
				break;
		}
		return null;
	};

	/*
     * Expose the previous Backbone.sync as Backbone.sync.previous in case
     * the caller wishes to switch provider.
     */
	sync.previous = previous;
	return sync;
})();