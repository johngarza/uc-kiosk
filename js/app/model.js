/* --- Date Format Constants --- */
var DT_FORMAT = 'YYYY-MM-DD';
var DT_FORMAT_FULL = 'ddd MMMM Do YYYY  |  h:mm a';
var TM_FORMAT = 'h:mma';
var UI_DT_FORMAT = 'yy-mm-dd';
var R25_DT_FORMAT = 'YYYY-MM-DDTHH:mm:ssZZ';
var TM_SPAN_FORMAT = '';
/* --- R25WS Constants --*/
var r25wsConfig = {
	base_uri: "cfc/",
	all_events: "events.cfc?wsdl&method=allEvents",
	major_events: "events.cfc?wsdl&method=majorEvents"
}

/* ---- MODELS ----*/
window.AppState = Backbone.Model.extend({
	defaults: {
		floor: "1",
		room: "1",
		kiosk: "1",
		current_time: moment().format(DT_FORMAT_FULL),
		current_view: "map",
		current_location: null,
		filter: "all",
		kiosks: {
			"kiosk1" : {
				title: "You Are Here",
				content: "UC North - 1st Floor",
				placement: "top"
			},
			"kiosk2" : {
				title: "You Are Here",
				content: "UC North - 1st Floor",
				placement: "right"
			},
			"kiosk3" : {
				title: "You Are Here",
				content: "HEB UC - 1st Floor",
				placement: "left"
			}
		}
	}
});

window.LocationData = Backbone.Model.extend({
	defaults: {
		rooms : {
			"Room1" : {
				id : "345",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Acacia Room",
				room: "UC 2.03.04",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Room2" : {
				id : "380",
				events: true,
				floor: "2",
				placement: "right",
				name: "Anaqua Room",
				room: "UC 2.03.08",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Room3" : {
				id : "381",
				events: true,
				floor: "2",
				placement: "right",
				name: "Ash Room",
				room: "UC 2.03.06",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Room4" : {
				id : "1128",
				events: true,
				floor: "1",
				placement: "left",
				name: "Ballroom 1",
				room: "HUC 1.106",
				bldg: "hUC",
				alpha: "AC"
			},
			"Room5" : {
				id : "1129",
				events: true,
				floor: "1",
				placement: "left",
				name: "Ballroom 2",
				room: "HUC 1.104",
				bldg: "hUC",
				alpha: "AC"
			},
			"Room6" : {
				id : "1205",
				events: true,
				floor: "1",
				placement: "top",
				name: "Bexar Room",
				room: "HUC 1.102",
				bldg: "hUC",
				alpha: "AC"
			},
			"Room7" : {
				id : "382",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Buckeye Room",
				room: "UC 2.01.32",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Room8" : {
				id : "1136",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Cameron Room",
				room: "HUC 2.216",
				bldg: "hUC",
				alpha: "AC"
			},
			"Room9" : {
				id : "383",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Chicano/a Room",
				room: "UC 2.01.40",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Room10" : {
				id : "385",
				events: true,
				floor: "2",
				placement: "left",
				name: "Denman Ballroom",
				room: "UC 2.01.28",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Room11" : {
				id : "1204",
				events: true,
				floor: "2",
				placement: "top",
				name: "Harris Room",
				room: "HUC 2.212",
				bldg: "hUC",
				alpha: "DI"
			},
			"Room12" : {
				id : "384",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Hawthorne Room",
				room: "UC 2.01.34",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Room13" : {
				id : "1134",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Hidalgo Room",
				room: "HUC 2.214",
				bldg: "hUC",
				alpha: "DI"
			},
			"Room14" : {
				id : "386",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Magnolia Room",
				room: "UC 2.01.30",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Room15" : {
				id : "387",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Mesquite Room",
				room: "UC 2.01.24",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Room16" : {
				id : "1137",
				events: true,
				floor: "2",
				placement: "top",
				name: "Montgomery Room",
				room: "HUC 2.214A.1",
				bldg: "hUC",
				alpha: "JR"
			},
			"Room17" : {
				id : "1135",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Nueces Room",
				room: "HUC 2.216",
				bldg: "hUC",
				alpha: "JR"
			},
			"Room18" : {
				id : "388",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Oak Room",
				room: "UC 2.01.20",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Room19" : {
				id : "389",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Paloverde Room",
				room: "UC 2.01.36",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Room20" : {
				id : "390",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Pecan Room",
				room: "UC 2.01.26",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Room21" : {
				id : "391",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Redbud Room",
				room: "UC 2.01.38",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Room22" : {
				id : "392",
				events: true,
				floor: "2",
				placement: "right",
				name: "Retama Auditorium",
				room: "UC 2.02.02",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Room23" : {
				id : "1133",
				events: true,
				floor: "2",
				placement: "top",
				name: "Travis Room",
				room: "HUC 2.202",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Room24" : {
				id : "393",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Wild Persimmon Room",
				room: "UC 2.03.02",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Room25" : {
				id : "394",
				events: true,
				floor: "2",
				placement: "right",
				name: "Willow Room",
				room: "UC 2.02.12",
				bldg: "ucNorth",
				alpha: "SZ"
			}
		},
		offices: {
			"Office1" : {
				id : "1",
				events: false,
				floor: "2",
				placement: "right",
				name: "Associate Dean of Students",
				room: "UC 2.01.05",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Office2" : {
				id : "3",
				events: false,
				floor: "1",
				placement: "top",
				name: "Campus Activities Board",
				room: "HUC 1.220",
				bldg: "hUC",
				alpha: "AC"
			},
			"Office3" : {
				id : "3",
				events: false,
				floor: "2",
				placement: "left",
				name: "Career Center",
				room: "UC 2.02.04",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Office4" : {
				id : "4",
				events: false,
				floor: "1",
				placement: "left",
				name: "Events Management & Conference Services",
				room: "UC 1.02.04",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Office5" : {
				id : "5",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Gallery 23",
				room: "UC 1.02.23",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Office6" : {
				id : "6",
				events: false,
				floor: "1",
				placement: "top",
				name: "Greek Councils",
				room: "HUC 1.226",
				bldg: "hUC",
				alpha: "DI"
			},
			"Office7" : {
				id : "7",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Honors Alliance",
				room: "HUC 1.228",
				bldg: "hUC",
				alpha: "DI"
			},
			"Office8" : {
				id : "8",
				events: false,
				floor: "2",
				placement: "bottom",
				name: "Inclusion and Community Engagement",
				room: "UC 2.01.04",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Office9" : {
				id : "9",
				events: false,
				floor: "2",
				placement: "right",
				name: "Student Conduct & Community Standards",
				room: "UC 2.0218",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Office10" : {
				id : "10",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Student Activities Office",
				room: "HUC 1.210",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Office11" : {
				id : "11",
				events: false,
				floor: "1",
				placement: "top",
				name: "Special Events Center & UTSA Ambassadors",
				room: "HUC 1.224",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Office12" : {
				id : "12",
				events: false,
				floor: "1",
				placement: "top",
				name: "Student Government Association",
				room: "HUC 1.214",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Office13" : {
				id : "13",
				events: false,
				floor: "1",
				placement: "top",
				name: "Student Leadership Center",
				room: "HUC 1.002",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Office14" : {
				id : "14",
				events: false,
				floor: "2",
				placement: "left",
				name: "Student Life",
				room: "UC 2.02.18",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Office15" : {
				id : "15",
				events: false,
				floor: "2",
				placement: "bottom",
				name: "Student Organization Complex",
				room: "HUC 2.206",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Office16" : {
				id : "16",
				events: false,
				floor: "1",
				placement: "left",
				name: "UC Administration",
				room: "UC 1.02.04",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Office17" : {
				id : "17",
				events: false,
				floor: "1",
				placement: "left",
				name: "UC Program Council",
				room: "UC 1.02.08",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Office18" : {
				id : "18",
				events: false,
				floor: "1",
				placement: "top",
				name: "VOICES",
				room: "HUC 1.216",
				bldg: "hUC",
				alpha: "SZ"
			}
		},
		lounges: {
			"Lounge1" : {
				id : "824",
				events: true,
				floor: "2",
				placement: "top",
				name: "Airport Lounge",
				room: "UC 2.02.00D",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Lounge2" : {
				id : "2",
				events: false,
				floor: "2",
				placement: "top",
				name: "Alamo Lounge",
				room: "HUC 2.214A",
				bldg: "hUC",
				alpha: "AC"
			},
			"Lounge3" : {
				id : "1203",
				events: true,
				floor: "1",
				placement: "left",
				name: "Ballroom Galleria",
				room: "HUC 1.100B",
				bldg: "hUC",
				alpha: "AC"
			},
			"Lounge4" : {
				id : "1217",
				events: true,
				floor: "2",
				placement: "bottom",
				name: "Denman Galleria",
				room: "UC 2.01",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Lounge5" : {
				id : "657",
				events: true,
				floor: "1",
				placement: "bottom",
				name: "Food Court",
				room: "UC 1.01.30",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Lounge6" : {
				id : "835",
				events: true,
				floor: "1",
				placement: "bottom",
				name: "Fountain Courtyard",
				room: "UC 1.01",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Lounge7" : {
				id : "821",
				events: true,
				floor: "2",
				placement: "right",
				name: "Retama Galleria",
				room: "UC 2.02",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Lounge8" : {
				id : "8",
				events: false,
				floor: "2",
				placement: "top",
				name: "Roost Gameroom",
				room: "HUC 2.22",
				bldg: "hUC",
				alpha: "JR"
			},
			"Lounge9" : {
				id : "1268",
				events: true,
				floor: "1",
				placement: "bottom",
				name: "Rowdy Lawn",
				room: "UC",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Lounge10" : {
				id : "654",
				events: true,
				floor: "1",
				placement: "bottom",
				name: "Ski Lodge",
				room: "UC 1.01",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Lounge11" : {
				id : "11",
				events: false,
				floor: "2",
				placement: "top",
				name: "Tejas Lounge",
				room: "HUC 2.208",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Lounge12" : {
				id : "1216",
				events: true,
				floor: "1",
				placement: "bottom",
				name: "UC Lawn",
				room: "HUC",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Lounge13" : {
				id : "1215",
				events: true,
				floor: "1",
				placement: "right",
				name: "UC Paseo",
				room: "",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Lounge14" : {
				id : "823",
				events: true,
				floor: "1",
				placement: "left",
				name: "Window Lounge",
				room: "UC 1.02.00C",
				bldg: "ucNorth",
				alpha: "SZ"
			}
		},
		dining: {
			"Dining1" : {
				id : "1",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Burger King",
				room: "UC 1.01.30",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Dining2" : {
				id : "2",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "C3 Convenience Store",
				room: "UC 1.00.38",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Dining3" : {
				id : "3",
				events: false,
				floor: "1",
				placement: "right",
				name: "Chili's Too",
				room: "UC 1.01.02",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Dining4" : {
				id : "4",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Panda Express",
				room: "UC 1.01.30",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Dining5" : {
				id : "5",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Starbucks",
				room: "UC 1.00.48",
				bldg: "ucNorth",
				alpha: "SZ"
			},
			"Dining6" : {
				id : "6",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Subway",
				room: "HUC 1.006",
				bldg: "hUC",
				alpha: "SZ"
			},
			"Dining7" : {
				id : "7",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Taco Cabana",
				room: "UC 1.01.30",
				bldg: "ucNorth",
				alpha: "SZ"
			}
		},
		services: {
			"Service1" : {
				id : "1",
				events: false,
				floor: "1",
				placement: "right",
				name: "Bookstore",
				room: "UC 1.02.02",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Service2" : {
				id : "2",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Campus Technology Store",
				room: "UC 1.00.46",
				bldg: "ucNorth",
				alpha: "AC"
			},
			"Service3" : {
				id : "3",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Forst Bank",
				room: "UC 1.00.46",
				bldg: "ucNorth",
				alpha: "DI"
			},
			"Service4" : {
				id : "4",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Princeton Review",
				room: "UC 1.04.02",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Service5" : {
				id : "5",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Rios Golden Cut Salons",
				room: "UC 1.00.42",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Service6" : {
				id : "6",
				events: false,
				floor: "1",
				placement: "bottom",
				name: "Roadrunner Express",
				room: "UC 1.00.43",
				bldg: "ucNorth",
				alpha: "JR"
			},
			"Service7" : {
				id : "7",
				events: false,
				floor: "1",
				placement: "top",
				name: "The UPS Store",
				room: "UC 1.04.02",
				bldg: "ucNorth",
				alpha: "SZ"
			}
		}
	}
});




