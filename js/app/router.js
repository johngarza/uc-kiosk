/* ---- ROUTER/CONTROLLER ---- */
var UCKioskRouter = Backbone.Router.extend({
	_location_data: null,
	_map_view: null,
	_app_state: null,
	_locations_view: null,
	_events_view: null,
	_header_view: null,
	_footer_view: null,
	_modal_view: null,

	routes: {
		"": "index",
		"map": "map",
		"locations": "locations",
		//"events": "events",
    "idle": "doIdle",
		"kiosk/:kiosk_id": "kiosk",
		"map/:type/:target_id":"showLocation"
		//"reload/:rsrv_id/:start_dt": "reload"
	},
	initialize: function(options) {
		this._location_data = options.location_data;
		this._app_state = options.app_state;
		this._modal_view = new ModalView({el: $("#modal"), model: this._app_state});
		this._modal_view.render();
		this._header_view = new HeaderView({el: $("#header"), model: this._app_state});
		this._footer_view = new FooterView({el: $("#footer"), model: this._app_state});
		this._map_view = new MapView({el: $("#display"), model: this._app_state});
		this._locations_view = new LocationsView({el: $("#display"), model: this._app_state, location_data: this._location_data});
		this._events_view = new EventsView({el: $("#display"), model: this._app_state, location_data: this._location_data });
	},
	index: function() {
		this._app_state.set({"current_view": "map"})
		app.navigate("map", true);
	},
	kiosk: function(kiosk_id) {
		var new_kiosk = "" + kiosk_id;
		var current_kiosk = this._app_state.get("kiosk");
		if (current_kiosk != new_kiosk) {
			this._app_state.set({"kiosk": new_kiosk});
		}
		app.navigate("map", true);
	},
	locations: function() {
		this._app_state.set({"current_view": "locations", "filter" : "all"})
		this._header_view.render();
		this._locations_view.render();
		this._footer_view.render();
	},
  doIdle: function() {
		console.log("router: doIdle");
		this._modal_view.render();
		this._app_state.set({"current_view": "map"})
		app.navigate("map", true);
		/*
    var options = {
      backdrop : true,
      keyboard: false,
      show: true,
      remote: false
    };
    $('#modal').modal(options)
		*/
  },
	events: function() {
		var eventsRouteThis = this;
		this._app_state.set({"current_view": "events", "filter" : "all"})
		$.ajax({
			url: r25wsConfig['base_uri'] + r25wsConfig['all_events'],
			method: "GET",
			dataType: "json",
			success: function(event_data) {
				event_data.sort(startCompare);
				eventsRouteThis._events_view.updateEvents(event_data);
			},
			complete: function() {
				eventsRouteThis._events_view.render();
			}
		});
		this._header_view.render();
		this._events_view.render();
		this._footer_view.render();
	},
	showLocation: function(type, target_id) {
		this._app_state.set({"current_view": "map"})
		this._header_view.render();
		this._map_view.render();
		this._footer_view.render();
	},
	map: function() {
		this._app_state.set({"current_view": "map", "current_location" : null})
		this._header_view.render();
		this._map_view.render();
		this._footer_view.render();
	},
	updateEvents: function(new_event_data) {
		this._events_view.updateEvents(new_event_data);
	}
});
