// Load the application once the DOM is ready, using `jQuery.ready`:
$(function() {
  //instantiate our model
  window.LocationData = new LocationData();
  window.AppState = new AppState();
  window.app = new UCKioskRouter({
    location_data: window.LocationData,
    app_config: window.AppConfig,
    app_state: window.AppState
  });
  Backbone.history.start();
});

var carousel = '';

var normalize_orgs = function(r25_obj) {
  var newItem = { label: r25_obj.organization_title._text, value: r25_obj.organization_id._text };
  return newItem;
};

var time_print = function(date_str) {
  var dt = moment(date_str);
  return dt.format(TM_FORMAT);
};

var normalize_contacts = function(r25_obj) {
  var newItem = {
    label: r25_obj.contact_name,
    value: r25_obj.contact_id
  };
  return newItem;
};

var update_time = function() {
  window.AppState.set({current_time: moment().format(DT_FORMAT_FULL)});
};

var startCompare = function(a, b) {
  if (a.reservation_start_dt < b.reservation_start_dt) return -1;
  if (a.reservation_start_dt > b.reservation_start_dt) return 1;
  return 0;
};

var update_events = function() {
  var _this = this;
  var new_event_data = [];
  $.ajax({
    url: r25wsConfig['base_uri'] + r25wsConfig['all_events'],
    method: 'GET',
    dataType: 'json',
    success: function(event_data) {
      window.app._this(event_data);
    },

    complete: function() {
    }
  });
};

var clock_interval = window.setInterval(update_time, 3000000);
var events_interval = window.setInterval(update_events, 600000);

var idleTimer = null;
var idleState = false;
var idleWait = 25000;

var options = {
  backdrop: true,
  keyboard: false,
  show: false,
  remote: false
};

(function($) {
  $(document).ready(function() {
    $('*').bind('mousemove keydown scroll', function() {
      clearTimeout(idleTimer);
      if (idleState == true) {
        // Reactivated event
        //$("modal").append("<p>Welcome Back.</p>");
        $('#modal').modal('hide');
      }

      idleState = false;
      idleTimer = setTimeout(function() {
        // Idle Event
        $('#modal').modal(options);
        $('#modal').on('shown', function() {
          carousel = $('.carousel').carousel({
            interval: 10000
          });
        });

        $('#modal').modal('show');
        idleState = true;
      }, idleWait);
    });

    $('body').trigger('mousemove');
  });
})(jQuery);
