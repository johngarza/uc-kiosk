<cfcontent type="text/xml" />
<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]><cfinvoke webservice="#Application.baseinvocation#events.cfc?wsdl" method="allEvents" returnvariable="strg"></cfinvoke>	
<major_events>
<cfoutput>
<cfloop array="#strg#" index="item">
	<event>
		<name><![CDATA[#item.event.event_name._text# ]]></name>
		<org><![CDATA[#item.event.organization_name._text# ]]></org>
		<start>#item.event.event_start_dt._text#</start>
		<end>#item.event.event_end_dt._text#</end>
		<location>#item.spaces.space_name._text#</location>
	</event>
</cfloop>
</cfoutput>
</major_events>